//
//  MShare.m
//  NarakCamera
//
//  Created by mookapook on 4/19/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import "MShare.h"
#import "MAppDelegate.h"
#import <Social/Social.h>



#import "Line.h"

#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"

/** test commit by Tharin **/
/** test commit by Payak **/
/** test commit by Tharin II **/
/** test commit by Tharin III **/
/** test commit by Payak II**/

@interface MShare ()

@end

@implementation MShare

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(showAlertView:)];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    self.navigationController.navigationBar.tintColor =  [[UIColor alloc] initWithRed:204.0 / 255 green:0.0 / 255 blue:102.0 / 255 alpha:1.0];
    
    self.title = @"Share";
    [self updateView];

    _tableview.delegate =self;
    _tableview.dataSource =self;
    
    MAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    
    if (!appDelegate.session.isOpen) {
        appDelegate.session = [[FBSession alloc] init];
        NSLog(@"%d - %d",FBSessionStateCreatedTokenLoaded,FBSession.activeSession.state);
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            NSLog(@"xxx");
            // even though we had a cached token, we need to login to make the session usable
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                                 FBSessionState status,
                                                                 NSError *error) {
                // we recurse here, in order to update buttons and labels
                [self updateView];
            }];
            
        }
    }
    _shareGO = @[@"Facebook",@"Instagram",@"Twitter",@"Line",@"Download",@"Save to Camera Roll"];
    
    
    NSString *documentDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    // create directory named "test"
    NSArray *paths = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentDirPath error:nil];
    
    NSLog(@"%d",paths.count);
    
    
    //BOOL isDir;
    NSMutableArray *directoriesOfFolder = [[NSMutableArray alloc] initWithCapacity:100];
    
    for (NSString *path in paths) {
        NSLog(@"apath: %@", path);
        
        BOOL isDir;
        NSString * fullPath = [documentDirPath stringByAppendingPathComponent:path];
        if ([[NSFileManager defaultManager] fileExistsAtPath:fullPath isDirectory:&isDir] &&isDir) {
            [directoriesOfFolder addObject:path];
            NSLog(@"directoriesOfFolder %@", directoriesOfFolder);
        }
    }
    
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void) updateView
{
   MAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (appDelegate.session.isOpen) {
        NSLog(@"Load");
        //_sharebtn.titleLabel.text = @"Share  Facebook";
            [FBSession setActiveSession:appDelegate.session];  
    }
    else
    {
        NSLog(@"Not Load");
    }
}

-(void)showAlertView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return _shareGO.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    
    // Configure the cell...
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        
        //cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = _shareGO[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0)
    {
    [self sharefacebook];
    }
    else if(indexPath.row == 1)
    {
        [self instragrame];
    }
    else if(indexPath.row == 2)
    {
        [self twitter];
    }
    else if(indexPath.row == 3)
    {
        if ([self checkIfLineInstalled]) {
            [Line shareImage:_Image];
            
            
        }
    }
    else if(indexPath.row == 4)
    {
        [self download];
    }
    else
    {
        [self savecamera];
    }
}

- (NSString *)_cachesPath:(NSString *)directory {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSLog(@"d %@",documentsDirectory);
    
    
    
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) lastObject]
                      stringByAppendingPathComponent:@"com.samsoffes.ssziparchive.tests"];
	if (directory) {
		path = [documentsDirectory stringByAppendingPathComponent:directory];
	}
    NSLog(@"d %@",path);
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if (![fileManager fileExistsAtPath:path]) {
		[fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
	return path;
}


-(BOOL)deleteFile:(NSString *)filename{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *myPathDocs = [documentsDirectory stringByAppendingPathComponent:filename];
    NSLog(@"[deleteFile] *myPathsDocs: %@", myPathDocs);
    
    NSLog(@"[deleteFile] about to delete file");
    //delete file
    BOOL success = [fileManager removeItemAtPath:myPathDocs error:NULL];
    // NSLog(@"[deleteFile] success? %@", success);
    [self.HUD setHidden:YES];
    NSLog(@"Success: %@", success ? @"YES" : @"NO");
    return success;
}

- (void)download {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://phs.googlecode.com/files/Download%20File%20Test.zip"]];//@"http://www.prakit.in.th/databasexcode.zip"]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"filename.zip"];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Successfully downloaded file to %@", path);
        
        // NSString *zipPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"filename" ofType:@"zip"];
        NSString *outputPath = [self _cachesPath:@"Stamp"];
        // NSString *destinationPath = @"zip1";
        // [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];
        
        
        NSError* error = nil;
        BOOL ok = [SSZipArchive unzipFileAtPath:path toDestination:outputPath overwrite:YES password:nil error:&error];
        NSLog(@"unzip status: %i %@", (int)ok, error);
        
        [self deleteFile:@"filename.zip"];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    [operation setDownloadProgressBlock:
     
     ^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
         NSLog(@"xxxx");
         float percentDone = totalBytesRead/(float)totalBytesExpectedToRead;
         
         //self.progressView.progress = percentDone;
         
         if(self.HUD==nil)
         {
             self.HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
             [self.navigationController.view addSubview:self.HUD];
             //self.HUD.mode=MBProgressHUDModeDeterminate;
             self.HUD.delegate = self;
             self.HUD.labelText = @"Download";
             //self.HUD.square = YES;
         
             [self.HUD show:true];
         }
         NSLog(@"percentDone = %f",percentDone);
         self.HUD.progress=percentDone;
     }];
    
    
    
    
    
    
    
    
    
    [operation start];
    
}


- (BOOL)checkIfLineInstalled {
    BOOL isInstalled = [Line isLineInstalled];
    
    if (!isInstalled) {
        [[[UIAlertView alloc] initWithTitle:@"Line is not installed." message:@"Please download Line from App Store, and try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
    return isInstalled;
}


-(void)postImageToFB
{
    
   // [[FBSession activeSession] accessToken];
    //    [FBRequestConnection startForUploadPhoto:_Image
    //                           completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
    //                               if (!error) {
    //                                   UIAlertView *tmp = [[UIAlertView alloc]
    //                                                       initWithTitle:@"Success"
    //                                                       message:@"Photo Uploaded"
    //                                                       delegate:self
    //                                                       cancelButtonTitle:nil
    //                                                       otherButtonTitles:@"Ok", nil];
    //
    //                                   [tmp show];
    //                                   //[tmp release];
    //                               } else {
    //                                   UIAlertView *tmp = [[UIAlertView alloc]
    //                                                       initWithTitle:@"Error"
    //                                                       message:@"Some error happened"
    //                                                       delegate:self
    //                                                       cancelButtonTitle:nil
    //                                                       otherButtonTitles:@"Ok", nil];
    //
    //                                   [tmp show];
    //                                   //[tmp release];
    //                               }
    //
    //                               //[self controlStatusUsable:YES];
    //                           }];
    //MAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
 
    
    self.HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:self.HUD];
	
    self.HUD.delegate = self;
    self.HUD.labelText = @"Share By You";
    self.HUD.detailsLabelText = @"กำลัง ส่งข้อมูล";
	self.HUD.square = YES;
	[self.HUD show:true];
     
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:@"#Narak Sticker" forKey:@"message"];
    [params setObject:UIImagePNGRepresentation(_Image) forKey:@"picture"];
    
    //_shareToFbBtn.enabled = NO; //for not allow ing multiple hits
    
    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
     {
         if (error)
         {
             //showing an alert for failure
             //[self alertWithTitle:@"Facebook" message:@"Unable to share the photo please try later."];
             
             //[self sharefacebook];
             
             UIAlertView *tmp = [[UIAlertView alloc]
                                 initWithTitle:@"Error"
                                 message:@"Unable to share the photo please try later."
                                 delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:@"Ok", nil];
             
             [tmp show];
             
             [self.HUD setHidden:YES];
         }
         else
         {
             //showing an alert for success
             //[UIUtils alertWithTitle:@"Facebook" message:@"Shared the photo successfully"];
             
             
             [self.HUD setHidden:YES];
             
             UIAlertView *tmp = [[UIAlertView alloc]
                                 initWithTitle:@"Success"
                                 message:@"Photo Uploaded"
                                 delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:@"Ok", nil];
             
             [tmp show];
             
         }
         //_shareToFbBtn.enabled = YES;
     }];
    
//    [FBRequestConnection startForUploadPhoto:_Image
//                           completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                               
//                               
//                               if(error)
//                               {
//                                   NSLog(@"error");
//                               }
//                               else {
//                               
//                               [self.HUD setHidden:YES];
//                               
//                                            UIAlertView *tmp = [[UIAlertView alloc]
//                                                                initWithTitle:@"Success"
//                                                                message:@"Photo Uploaded"
//                                                                delegate:self
//                                                                cancelButtonTitle:nil
//                                                                otherButtonTitles:@"Ok", nil];
//                               
//                                            [tmp show];
//                               }
//
//                           }];
//    
//    
//    
 
    

}


- (void) sharefacebook

{
    
    
}
-(void)openSessionForPublishPermissions
{
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
    {
        
    
    [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"   "]
                                          defaultAudience:FBSessionDefaultAudienceFriends
                                        completionHandler:^(FBSession *session, NSError *error) {
                                            if (!error) {
                                                // Now have the permission
                                               [self postImageToFB];
                                            } else {
                                                // Facebook SDK * error handling *
                                                // if the operation is not user cancelled
                                                if (error.fberrorCategory != FBErrorCategoryUserCancelled) {
                                                    [self presentAlertForError:error];
                                                }
                                            }
                                        }];
    }
    else
    {
       // [self postImageToFB];
    }
}

- (void) presentAlertForError:(NSError *)error {
    // Facebook SDK * error handling *
    // Error handling is an important part of providing a good user experience.
    // When fberrorShouldNotifyUser is YES, a fberrorUserMessage can be
    // presented as a user-ready message
    if (error.fberrorShouldNotifyUser) {
        // The SDK has a message for the user, surface it.
        [[[UIAlertView alloc] initWithTitle:@"Something Went Wrong"
                                    message:error.fberrorUserMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    } else {
        NSLog(@"unexpected error:%@", error);
    }
}


-(void) twitter
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"#Narak Sticker"];
        [tweetSheet addImage:_Image];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You Set Twitter account setup On Menu Setting"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

-(void) savecamera
{
    UIImageWriteToSavedPhotosAlbum(_Image, nil, nil, nil);
    UIAlertView *tmp = [[UIAlertView alloc]
                        initWithTitle:@"Success"
                        message:@"Photo Save"
                        delegate:self
                        cancelButtonTitle:nil
                        otherButtonTitles:@"Ok", nil];
    
    [tmp show];
}

-(void) instragrame
{

//[self storeimage];
//NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
//if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
//{
//
//    CGRect rect = CGRectMake(0 ,0 , 612, 612);
//    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/15717.ig"];
//
//    NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];
//    UIDocumentInteractionController *dic = [[UIDocumentInteractionController alloc] init];
//
//    dic.UTI = @"com.instagram.photo";
//    dic.delegate=self;
//    dic = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
//    dic=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
//    dic.delegate=self;
//    [dic presentOpenInMenuFromRect: rect    inView: self.view animated: YES ];
//    [[UIApplication sharedApplication] openURL:instagramURL];
//}
//else
//{
//    //   NSLog(@"instagramImageShare");
//    UIAlertView *errorToShare = [[UIAlertView alloc] initWithTitle:@"Instagram unavailable " message:@"You need to install Instagram in your device in order to share this image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    
//    errorToShare.tag=3010;
//    [errorToShare show];
//}
    
//    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
//    
//    if([[UIApplication sharedApplication] canOpenURL:instagramURL])
//    {
//        NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
//        NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.ig"];
//        NSData *imageData=UIImagePNGRepresentation(_Image);
//        [imageData writeToFile:saveImagePath atomically:YES];
//        
//        NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
//        
//        UIDocumentInteractionController *docController=[[UIDocumentInteractionController alloc]init];
//        docController.delegate=self;
//        //[docController retain];
//        docController.UTI=@"com.instagram.photo";
//        
//        docController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:@"Image Taken via @App",@"InstagramCaption", nil];
//        
//        [docController setURL:imageURL];
//        
//        [docController presentOpenInMenuFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];  //Here try which one is suitable for u to present the doc Controller. if crash occurs
//
//       //[[UIApplication sharedApplication] openURL:instagramURL];
//        
//    }
//    else
//    {
//        UIAlertView *errorToShare = [[UIAlertView alloc] initWithTitle:@"Instagram unavailable " message:@"You need to install Instagram in your device in order to share this image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [errorToShare show];
//        
//    }
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        CGRect rect = CGRectMake(0,0,0,0);
        //CGRect cropRect=CGRectMake(0,0,612,612);
        NSString *jpgPath=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/test.igo"];
       // CGImageRef imageRef = CGImageCreateWithImageInRect([imgViewPost.image CGImage], cropRect);
        //UIImage *img = [[UIImage alloc] initWithCGImage:imageRef];
        //CGImageRelease(imageRef);
        //NSData *imageData=UIImagePNGRepresentation(_Image);
        [UIImageJPEGRepresentation(_Image, 1.0) writeToFile:jpgPath atomically:YES];
        NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@",jpgPath]];
        
        
        dicont = [UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
        
        
        //dicont = [[UIDocumentInteractionController alloc] init];
        dicont.UTI = @"com.instagram.exclusivegram";
        //dicont = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
        dicont.annotation = [NSDictionary dictionaryWithObject:@"#Narak Sticker" forKey:@"InstagramCaption"];
        [dicont presentOpenInMenuFromRect: rect  inView: self.view animated: YES ];
    }
    else
    {
        UIAlertView *errorToShare = [[UIAlertView alloc] initWithTitle:@"Instagram unavailable " message:@"You need to install Instagram in your device in order to share this image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorToShare show];
    }
    
}
- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    
    return interactionController;
}

- (void) storeimage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"15717.ig"];
    //UIImage *NewImg=[self resizedImage:imageCapture :CGRectMake(0, 0, 612, 612) ];
    NSData *imageData = UIImagePNGRepresentation(_Image);
    [imageData writeToFile:savedImagePath atomically:NO];
}


-(void)promptUserWithAccountNameForStatusUpdate {
    //[self controlStatusUsable:NO];
    [[FBRequest requestForMe] startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
         if (!error) {
             
             NSLog(@"%@",user.name);
             //_sharebtn.titleLabel.text = @"Share  Facebook";
             [self postImageToFB];
             
             //[tmp release];
             
         }
         
         //[self controlStatusUsable:YES];
     }];
}

- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller
{
    
}

- (BOOL)documentInteractionController:(UIDocumentInteractionController *)controller canPerformAction:(SEL)action
{
    //    NSLog(@"5dsklfjkljas");
    return YES;
}

- (BOOL)documentInteractionController:(UIDocumentInteractionController *)controller performAction:(SEL)action
{
    //    NSLog(@"dsfa");
    return YES;
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
{
    //    NSLog(@"fsafasd;");
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableview:nil];
    [super viewDidUnload];
}
@end
