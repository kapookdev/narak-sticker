//
//  MLastView.h
//  NarakCamera
//
//  Created by mookapook on 4/10/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MListStamp.h"
@protocol return_last_stamp <NSObject>
-(void) back_stamp:(int)obj withpic:(int)pic;
@end
@interface MLastView : UIViewController<return_stamp>
{
    NSString *name;
    NSString *email;
    NSString *fid;
    NSString *token;
}
@property(nonatomic,assign) id <return_last_stamp> delegate;
@property (weak, nonatomic) IBOutlet UIButton *LoginFaceBook;
@property (strong, nonatomic)  UIImage *Image;

@property (weak, nonatomic) IBOutlet UIButton *ShareFaceBook;
@property (weak, nonatomic) IBOutlet UIButton *sharebtn;
@property (strong, nonatomic)  UIButton *SharePhoto;
- (IBAction)PostPhoto:(UIButton *)sender;
- (IBAction)SaveCamera:(id)sender;
@end
