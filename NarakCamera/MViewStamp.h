//
//  MViewStamp.h
//  NarakCamera
//
//  Created by mookapook on 4/7/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MViewStamp2.h"
@protocol returnstamp <NSObject>
-(void) gotoStamp:(int) obj;
@end
@interface MViewStamp : UIViewController<returnstamp2>
- (IBAction)SEND:(id)sender;
@property(nonatomic,assign) id <returnstamp> delegate;
@property (weak, nonatomic) IBOutlet UIButton *STAMP2;
@property (strong, nonatomic) IBOutlet UIView *View1;
- (IBAction)Stampv1:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *View2;
- (IBAction)STAMP2:(id)sender;
@end
