//
//  MViewController.m
//  NarakCamera
//
//  Created by mookapook on 4/5/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import "MViewController.h"
#import "MViewPicture.h"
@interface MViewController ()

@end

@implementation MViewController
@synthesize imagePicker;
- (void)viewDidLoad
{
    [super viewDidLoad];
        NSLog(@"%f",KDeviceHeight);
    self.title = @"Narak Camera";
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bg-home@2x.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    NSString *Adsurl = [NSString stringWithFormat:@"http://iosad.kapook.com"];
    
    // Load URL
    
    //Create a URL object.
    NSURL *urlads = [NSURL URLWithString:Adsurl];
    
    //URL Requst Object
    NSURLRequest *requestObjads = [NSURLRequest requestWithURL:urlads];
    
    //Load the request in the UIWebView.
    [_webview loadRequest:requestObjads];

	// Do any additional setup after loading the view, typically from a nib.
}


-(void) viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:animated];
        
}






-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        //        NSString *string = [[NSBundle mainBundle] pathForResource:@"1110" ofType:@"JPEG"];
        //        currentImage = [UIImage imageWithContentsOfFile:string];
        //        rootImageView.image = currentImage;
        //        seg.userInteractionEnabled = YES;
        //        [self.view addSubview:rootImageView];
        
        imagePicker = [[UIImagePickerController alloc] init];//图像选取器
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//打开相册
        imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;//过渡类型,有四种
        imagePicker.allowsEditing = YES;//禁止对图片进行编辑
        
        [self presentViewController:imagePicker animated:YES completion:nil];//打开模态视图控制器选择图像
        
    }
    if(buttonIndex == 1)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;//照片来源为相机
            imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            imagePicker.allowsEditing = YES;//禁止对图片进行编辑
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"CameraError" delegate:nil cancelButtonTitle:@"Cancle" otherButtonTitles:nil, nil];
            [alert show];
            //  [alert release];
        }
    }
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
    if (image == nil) {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    
    //UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];//获取图片
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);//将拍到的图片保存到相册
    }
    
    
    if(KDeviceHeight > 480)
    {
    currentImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(320, 320)];
    }
    else
    {
    
    currentImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(320, 320)];
    }
    //    currentImage = image;
    
    //调用imageWithImageSimple:scaledToSize:方法
    
    //[currentImage retain];
    /*
     rootImageView.image = currentImage;
     
     imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 45, 50)];
     rootImage = [UIImage imageNamed:@"110.png"];
     imageView.image = rootImage;
     [scrollerView addSubview:imageView];
     [imageView release];
     
     seg.userInteractionEnabled = YES;
     [self.view addSubview:rootImageView];
     */
    
    MViewPicture *p = [[MViewPicture alloc] initWithNibName:@"MViewPicture" bundle:nil];
    
    p.Image = currentImage;
    
    [self.navigationController pushViewController:p animated:YES ];
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];//关闭模态视图控制器
}

-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}



-(void)open//leftBarButton调用
{
    //[imageView removeFromSuperview];
    
    
    // [sheet release];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






// Post Status Update button handler; will attempt to invoke the native
// share dialog and, if that's unavailable, will post directly



- (IBAction)Open:(id)sender {
    /*UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"เลือก" delegate:self cancelButtonTitle:@"ยกเลิก" destructiveButtonTitle:nil otherButtonTitles:@"เลือกจาก Album",@"ถ่ายจากกล้อง", nil];//关闭按钮在最后
    
    sheet.delegate = self;
    sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;//样式
    [sheet showInView:self.view];//显示样式
    */
    
    imagePicker = [[UIImagePickerController alloc] init];//图像选取器
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//打开相册
    imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;//过渡类型,有四种
    imagePicker.allowsEditing = YES;//禁止对图片进行编辑
    
    [self presentViewController:imagePicker animated:YES completion:nil];
    
}

- (IBAction)Opencamera:(id)sender {
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;//照片来源为相机
        imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        imagePicker.allowsEditing = YES;//禁止对图片进行编辑
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        imagePicker = [[UIImagePickerController alloc] init];//图像选取器
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//打开相册
        imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;//过渡类型,有四种
        imagePicker.allowsEditing = YES;//禁止对图片进行编辑
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        //  [alert release];
    }
}


-(void)promptUserWithAccountNameForStatusUpdate {
    //[self controlStatusUsable:NO];
    [[FBRequest requestForMe] startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
         if (!error) {
             
             NSLog(@"%@",user.name);
             
            // [self postImageToFB];
             
             //[tmp release];
             
         }
         
         //[self controlStatusUsable:YES];
     }];
}


-(void)showAlertView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}





- (void)viewDidUnload {
    [self setWebview:nil];
    [super viewDidUnload];
}
     @end
