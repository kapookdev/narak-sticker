//
//  main.m
//  NarakCamera
//
//  Created by mookapook on 4/5/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MAppDelegate class]));
    }
}
