//
//  MViewPicture.h
//  Narak
//
//  Created by mookapook on 3/28/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANDraggable.h"
#import "MViewStamp.h"
#import "MShare.h"
#import "MLastView.h"
#import "MViewStamp2.h"
@interface MViewPicture : UIViewController<SEDraggableEventResponder,  UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,UIGestureRecognizerDelegate,returnstamp2>
{
    int c;
    int num;
    int color;
    int tag;
    int tagview;
    int tagbutton;
    float hi;
        float hitable;
    CGColorRef      segmentColor;
    float deltaAngle;
    CGPoint prevPoint;
    CGAffineTransform startTransform;
    UIImageView *resizeVw;
    CGFloat _lastScale;
    CGFloat _lastRotation;

    //UIButton *dotDelete;
}
@property (strong, nonatomic) IBOutlet UIView *Pic;
@property (strong, nonatomic) IBOutlet UIView *ViewText;
@property (strong, nonatomic)  UIButton *deletebutton;
@property (strong, nonatomic)  UIButton *dotDelete;

@property (strong,nonatomic)     NSArray *sticker;

@property (retain, nonatomic)  UIImageView *imageView;
@property (weak, nonatomic)  UIImage *Image;
@property (nonatomic, strong) UIImageView *frameImageView;
@property (nonatomic, strong) UITableView *filtersTableView;
@property (nonatomic, strong) UIView *filterTableViewContainerView;
@property (weak, nonatomic) IBOutlet UITextView *text;
@property (weak,nonatomic) UIView *selectedView;
@property (weak,nonatomic) UIImageView *selectedimageR;
@property (weak, nonatomic) IBOutlet UIView *topv;
@property (strong,nonatomic) UIButton *stampbtn;
@property (strong,nonatomic) UIButton *fillter;
@property (strong,nonatomic) UIButton *txtbtn;
@property (strong,nonatomic) UIButton *framebtn;
@property (strong,nonatomic) UIButton *backbtn;
@property (strong,nonatomic) UIButton *savebtn;
@property (strong, nonatomic) UIView *v;
@property (nonatomic, strong) UIPinchGestureRecognizer *localpinch;
@property (nonatomic, strong) UIRotationGestureRecognizer *localrotation;
@end
