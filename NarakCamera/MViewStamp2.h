//
//  MViewStamp2.h
//  NarakCamera
//
//  Created by mookapook on 4/9/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol returnstamp2 <NSObject>
-(void) gotoStamp2:(int) obj with:(int)tagstamp;
@end

@interface MViewStamp2 : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate,UIScrollViewDelegate>
{
    int tagstamp;
   // UIScrollView *scrollView;
}
@property (strong, nonatomic)  UIScrollView *scrollView;
@property (nonatomic, retain) UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UICollectionView *collection;
@property (strong,nonatomic)     NSArray *sticker;
@property(nonatomic,assign) id <returnstamp2> delegate;
- (IBAction)return:(id)sender;
- (IBAction)reloadstamp:(id)sender;
@end
