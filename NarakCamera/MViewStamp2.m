//
//  MViewStamp2.m
//  NarakCamera
//
//  Created by mookapook on 4/9/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import "MViewStamp2.h"
#import "MC.h"
#import <QuartzCore/QuartzCore.h>
@interface MViewStamp2 ()

@end

@implementation MViewStamp2

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)showAlertView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    float hs;
    if([[[UIDevice currentDevice] systemVersion] floatValue]>6.0f)hs=43;
    else hs=0;
    // 4
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44,
    
                                                                 self.view.frame.size.width,
                                                                     51)];
    _scrollView.backgroundColor = [[UIColor alloc] initWithRed:153.0 / 255 green:0.0 / 255 blue:76.0 / 255 alpha:1.0];
    //_scrollView.pagingEnabled = YES;
    
    NSArray *numberOfViews = @[@"rabbit0.png",@"panda0.png",@"dog0.png",@"cat0.png",@"monkey0.png"];
    
    _scrollView.delegate = self;
    //setup internal views
    CGFloat cx = 10;
    //NSInteger numberOfViews = 5;
    for (int i = 0; i < numberOfViews.count; i++) {
        UIButton *btnKategori=[UIButton buttonWithType:UIButtonTypeCustom];
        btnKategori.frame=CGRectMake(cx,1, 50, 50);
        btnKategori.titleLabel.font = [UIFont fontWithName:@"Arial-BoldItalicMT" size:16];
        cx+=65-2;
        [btnKategori setTitle:@"" forState:(UIControlState)UIControlStateNormal];
        [btnKategori setImage:[UIImage imageNamed:[numberOfViews objectAtIndex:i]] forState:UIControlStateNormal];
        [btnKategori.layer setBorderColor:[[UIColor blackColor] CGColor]];
        [[btnKategori layer] setCornerRadius:5.0f];
        [[btnKategori layer] setMasksToBounds:YES];
        [[btnKategori layer] setBorderWidth:2.0f];

        [btnKategori setTag:i+1];
        [btnKategori addTarget:self action:@selector(reloadstamp:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.scrollView addSubview:btnKategori];
    }
    //set the scroll view content size
    self.scrollView.contentSize = CGSizeMake(75*
                                             numberOfViews.count,
                                             51);
    //add the scrollview to this view
    [self.view addSubview:self.scrollView];
//    _pageControl = [[UIPageControl alloc] init];
//    _pageControl.frame = CGRectMake(130, 50, 50, 20);
//    _pageControl.numberOfPages = 2;
//    _pageControl.currentPage = 0;
//    self.pageControl.backgroundColor = [UIColor blackColor];
//    [self.view addSubview:_pageControl];
//       // [self.view addSubview:self.pageControl];
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = 45;
    NSInteger pageNumber = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    NSLog(@"%d %f",pageNumber,scrollView.contentOffset.x);
    _pageControl.currentPage = pageNumber;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(showAlertView:)];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    self.navigationController.navigationBar.tintColor =  [[UIColor alloc] initWithRed:204.0 / 255 green:0.0 / 255 blue:102.0 / 255 alpha:1.0];
    
    self.title = @"Sticker";

    _collection.dataSource = self;
    _collection.delegate = self;
    
    [self.collection registerNib:[UINib nibWithNibName:@"View" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    [_collection reloadData];
    _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"rabbit" ofType:@"plist"]];
    tagstamp = 1;
    //_scrollview = [[UIScrollView alloc] init];
    //UIScrollView *viewscroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    //[viewscroll setContentSize:CGSizeMake(431, 50)];
    //[viewscroll setBackgroundColor:[UIColor blackColor]];
   // [viewscroll setNeedsDisplay:YES];
    //[self.view addSubview:viewscroll];
     // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog(@"ddd %d",_sticker.count);
    return [_sticker count];
}
-(UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MC *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    //UIImage *image;
    //image = [UIImage imageNamed:_carImages[indexPath.row]];
    //myCell.im.image = image;
    //NSDictionary *obj = [_movies objectAtIndex:indexPath.row];
    
    // NSString *imageString = [obj objectForKey:@"pic"];
    
    NSString *picname = [NSString stringWithFormat:@"%@",_sticker[indexPath.row]];
    
    myCell.image.image = [UIImage imageNamed:picname];
    //myCell.image =
    
    return myCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",indexPath.row);
    [self.delegate gotoStamp2:indexPath.row with:tagstamp];
    [self dismissViewControllerAnimated:YES completion:nil];
//    
//    MWebController *m = [[MWebController alloc] initWithNibName:@"MWebController" bundle:nil];
//    NSDictionary *obj = [_movies objectAtIndex:indexPath.row];
//    m.url = [NSString stringWithFormat:@"http://www.nolimitbox.com/ajax_view_mobile2.php?id=%@",[obj objectForKey:@"id"]];
//    
//    [self.navigationController pushViewController:m animated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)return:(id)sender {
    //[self.delegate gotoStamp2:1];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)reloadstamp:(id)sender {
    
    NSLog(@"%d",[sender tag]);
    
    UIButton *button = (UIButton*)sender;
    UIButton *random;
     random = (UIButton *)[self.view viewWithTag:1];
    [random.layer setBorderColor:[[UIColor blackColor] CGColor]];
    
    random = (UIButton *)[self.view viewWithTag:2];
    [random.layer setBorderColor:[[UIColor blackColor] CGColor]];
    random = (UIButton *)[self.view viewWithTag:3];
    [random.layer setBorderColor:[[UIColor blackColor] CGColor]];
    random = (UIButton *)[self.view viewWithTag:4];
    [random.layer setBorderColor:[[UIColor blackColor] CGColor]];
     random = (UIButton *)[self.view viewWithTag:5];
    [random.layer setBorderColor:[[UIColor blackColor] CGColor]];
    
    
    
    
    [button.layer setBorderColor:[[UIColor blueColor] CGColor]];
    
    
    if([sender tag] == 1)
    {
    _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"rabbit" ofType:@"plist"]];
        tagstamp = 1;
        
    }
    else if([sender tag]==2)
    {
       _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"panda" ofType:@"plist"]];
                tagstamp = 2;
    }
    else if([sender tag]==3)
    {
        _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"dog" ofType:@"plist"]];
        tagstamp = 3;
    }
    else if([sender tag]==4)
    {
        _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"cat" ofType:@"plist"]];
        tagstamp = 4;
    }
    
    else
    {
        _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"monkey" ofType:@"plist"]];
                tagstamp = 5;
    }
    
    [_collection reloadData];
}
@end
