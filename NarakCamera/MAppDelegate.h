//
//  MAppDelegate.h
//  NarakCamera
//
//  Created by mookapook on 4/5/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MViewController;
@class DLCImagePickerController;
//extern NSString *const FBSessionStateChangedNotification;
@interface MAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MViewController *viewController;
@property (strong, nonatomic) DLCImagePickerController *DLCImagePickerController;
@property (strong, nonatomic) FBSession *session;
//- (void) closeSession;
//- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;

@end
