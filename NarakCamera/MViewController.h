//
//  MViewController.h
//  NarakCamera
//
//  Created by mookapook on 4/5/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
@interface MViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,FBLoginViewDelegate>
{
    UIImage *currentImage;
}
@property (nonatomic,retain) UIImagePickerController *imagePicker;
- (IBAction)Open:(id)sender;
- (IBAction)Opencamera:(id)sender;

@property (strong, nonatomic) id<FBGraphUser> loggedInUser;
@property (weak, nonatomic) IBOutlet UIWebView *webview;

@end
