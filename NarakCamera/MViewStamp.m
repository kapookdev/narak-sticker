//
//  MViewStamp.m
//  NarakCamera
//
//  Created by mookapook on 4/7/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import "MViewStamp.h"

@interface MViewStamp ()

@end

@implementation MViewStamp

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.modalTransitionStyle = UIModalTransitionStylePartialCurl;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)gotoStamp2:(int)obj
{
    [self.delegate gotoStamp:obj];
    NSLog(@"xxx");
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)SEND:(id)sender {
  //    [self.delegate gotoStamp:1];
//    MViewStamp2 *v = [[MViewStamp2 alloc] initWithNibName:@"MViewStamp2" bundle:nil];
//    v.delegate = self;
//    [self presentModalViewController:v animated:YES];
//    
     //[self dismissModalViewControllerAnimated:YES];
    self.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    self.view = _View2;
}
- (void)viewDidUnload {
    [self setSTAMP2:nil];
    [self setView1:nil];
    [self setView2:nil];
    [super viewDidUnload];
}
- (IBAction)STAMP2:(id)sender {
    [self.delegate gotoStamp:2];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (IBAction)Stampv1:(id)sender {
    [self.delegate gotoStamp:1];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
