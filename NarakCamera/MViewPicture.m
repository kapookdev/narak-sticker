//
//  MViewPicture.m
//  Narak
//
//  Created by mookapook on 3/28/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import "MViewPicture.h"
#import "ImageUtil.h"

#import "KTOneFingerRotationGestureRecognizer.h"
#import "ColorMatrix.h"
#import <QuartzCore/QuartzCore.h>
#define kFilterImageViewTag 99999
#define kFilterImageViewContainerViewTag 99998
#define kBlueDotImageViewOffset 25.0f
#define kFilterCellHeight 90.0f
#define kBlueDotAnimationTime 0.2f
#define kFilterTableViewAnimationTime 0.2f
#define kGPUImageViewAnimationOffset 27.0f
#define kMaxStickerWidth 100
#define degreesToRadians(x) (M_PI * x / 180.0)
#define wobj 54.0f

@interface MViewPicture ()

@end

@implementation MViewPicture
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)AddStamp:(int)sender {
    
    
    NSLog(@"%d",sender);
    NSString *picname ;
    [_dotDelete removeFromSuperview];
    if(num == 1)
    {
        //picname = [NSString stringWithFormat:@"stamp_%d.png",sender];
        picname = [NSString stringWithFormat:@"%@.png",_sticker[sender]];
    }
    else
    {
    if(sender > 29)
    {
        picname = [NSString stringWithFormat:@"stamp_%d.png",sender];
        //filterImageView.image = [UIImage imageNamed:picname];
        
    }
    else
    {
       picname = [NSString stringWithFormat:@"%d.png",sender];
        
        //filterImageView.image = [UIImage imageNamed:picname];
        
    }
    }
    
    for(int x=9999;x<tagbutton;x++)
    {
        //[self.view viewWithTag:x].hidden = YES;
        [self.view viewWithTag:x].hidden = YES;
        
        
        
        
    }
    
    //UIView *c = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    
    
    
    //NSString *picname = [NSString stringWithFormat:@"%d.png",sender];
    
    UIImage *tmpImage2 = [UIImage imageNamed:picname];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:tmpImage2];
    imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    CGFloat ratio = 150 / tmpImage2.size.width;
    imageView.frame = CGRectMake(0, 0, tmpImage2.size.width * ratio, tmpImage2.size.height * ratio);
    imageView.tag = tag;

    
    //[c addSubview:imageView];
    
    
    
   // CGSize s = CGSizeMake(tmpImage2.size.width * ratio,tmpImage2.size.height * ratio);
    
    
    
    
    //tag = tag+1;
    
    
    
//    UIPinchGestureRecognizer *tabshare =
//    [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];

    
       
//    UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    deleteBtn.frame = CGRectMake(150, -5, 20, 20);
//    deleteBtn.tag = tagbutton;
//    [deleteBtn setImage:[UIImage imageNamed:@"bin.png"]forState:UIControlStateNormal];
//    
//    deleteBtn.layer.shadowColor = [[UIColor blackColor] CGColor];
//    deleteBtn.layer.shadowOffset = CGSizeMake(0,4);
//    deleteBtn.layer.shadowOpacity = 0.3;
//    deleteBtn.userInteractionEnabled = YES;
    //[deleteBtn addGestureRecognizer:tabshare];
    
    
    //[deleteBtn addTarget:self action:@selector(plus:) forControlEvents:UIControlEventTouchUpInside];
    
   // [imageView insertSubview:deleteBtn belowSubview:_imageView];
    
    //[imageView addSubview:deleteBtn];
    
    //UIImageView *dragger2 = [[Draggable alloc] initWithFrame:cellRectangle2];
    //[dragger2 setImage:tmpImage2];
    //[dragger2 setUserInteractionEnabled:YES];
    
    
    //[_vv addSubview:dragger];
    // [_vv addSubview:dragger2];
    
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    
    // Images
    
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tmpImage2.size.width * ratio, tmpImage2.size.height * ratio)];
//    view.backgroundColor = [UIColor grayColor];
//    view.tag = tagview;
//    view.layer.anchorPoint = CGPointMake(0.5, 0.5);
//    
    
    _selectedView.backgroundColor = [UIColor clearColor];
    _selectedView.layer.borderColor = [UIColor clearColor].CGColor;
//    UIButton *button = nil;
//    NSLog(@"%d - %d",[_selectedView.subviews count],tagview);
//    
//
//    int i =  tagview;
//    for (UIView *view in [_selectedView subviews])
//    {
//        
//        NSLog(@"subview %d",i);
//       // button= (UIButton *)view;
//       // [button viewWithTag:i].hidden = YES;
//        //        if([view isKindOfClass:[UIButton class]]) {
//        //            //do whatever you want to do
//        //
//        //        }
//        //
//        // (view *)[_selectedView viewWithTag:i].hidden = YES;
//        
//        //subview.backgroundColor = [UIColor whiteColor];
//        
//        
//        
//        
//        i = i-1;
//        //subview = nil;
//        //NSLog(@"xxx");
//    }
//    
    
    //ANDraggable *dragView = [[ANDraggable alloc] initWithView:view];
    
   // ANDraggable *dragView = [[ANDraggable alloc] initWithImageView:imageView ];
    
    ANDraggable *dragView = [[ANDraggable alloc] initWithImageView:imageView];
    
    
    dragView.delegate = self;
    dragView.tag = tagview;
        
  // [self addRotationGestureToView:dragView];
   // [dragView addSubview:c];
    //dragView.tag = 1;
    [_imageView addSubview:dragView];
    [_imageView insertSubview:dragView belowSubview:_frameImageView];
    _selectedView = imageView;
   // _selectedimageR = resizeVw;

    
    //_selectedView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    
    _selectedView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.4];
    _selectedView.layer.borderColor = [UIColor whiteColor].CGColor;
    _selectedView.layer.borderWidth = 3.0f;
    //_selectedView.alpha = 0.5;
    _deletebutton.hidden = YES;
    tagbutton = tagbutton+1;
    tag = tag+1;
    tagview = tagview+1;
    //[self cview];
    
    if(_dotDelete == nil)
    {
        _dotDelete=[UIButton buttonWithType:UIButtonTypeCustom];
        _dotDelete.frame = CGRectMake(0, 0, 25, 25);
        //[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [_dotDelete setImage:[UIImage imageNamed:@"close_gold.png"]forState:UIControlStateNormal];
        _dotDelete.center=CGPointMake(_selectedView.bounds.origin.x+18, _selectedView.bounds.origin.y+15
                                      );
        _dotDelete.tag = 989999;
        _dotDelete.userInteractionEnabled = YES;
        //[dotDelete addGestureRecognizer:singleTap];
        
        [_dotDelete addTarget:self action:@selector(deletestamp:) forControlEvents:UIControlEventTouchUpInside];
        
        NSLog(@"View");
        
    }
    else
    {
        [_selectedView addSubview:_dotDelete];
        [_selectedView setHidden:NO];
        
    }
    
}

- (void)ADDFrame:(int)sender {
    
    
    NSLog(@"%d",sender);
    
    //NSString *picname = [NSString stringWithFormat:@"frame_%d.png",sender];
    
    
    NSString *picname = [NSString stringWithFormat:@"%@",_sticker[sender]];
    
    
    if(_frameImageView == nil)
    {
    _frameImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    _frameImageView.userInteractionEnabled = NO;
    [self.imageView addSubview:_frameImageView];
    }
    // _frameImageView.image = [UIImage imageNamed:[[NSArray arrayWithContentsOfFile:NIPathForBundleResource([NSBundle mainBundle], @"frames.plist")] firstObject]];
    
    if(sender > 0)
    {
    //[self.view addSubview:_frameImageView];
    //[self.view insertSubview:_deletebutton aboveSubview:_frameImageView];
        
    
    
    UIImage *tmpImage = [UIImage imageNamed:picname];
    _frameImageView.image = tmpImage;
    }
    else
        
    {
        [_frameImageView removeFromSuperview];
        _frameImageView = nil;
    }
    
}

- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer
{
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
}








#pragma mark - Filters TableView Delegate & Datasource methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kFilterCellHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"xxx");
    
    if(c == 2)
    {
        [self AddStamp:indexPath.row];
    }
    else if(c==3)
    {
        [self ADDFrame:indexPath.row];
    }
    
    else if(c==4)
    {
        color = indexPath.row;
        NSLog(@"%d",color);
        [self ADDText];
    }
    
    else
    {
        NSLog(@"%d",indexPath.row);
    if(indexPath.row == 0)
        _imageView.image = _Image;
    else if(indexPath.row == 1)
    {
         _imageView.image = [ImageUtil imageWithImage:_Image withColorMatrix:colormatrix_danya];
    }
    else if(indexPath.row == 2)
    {
        _imageView.image = [ImageUtil imageWithImage:_Image withColorMatrix:colormatrix_lomo];
    }
    else if(indexPath.row == 3)
    {
        _imageView.image = [ImageUtil imageWithImage:_Image withColorMatrix:colormatrix_huajiu];
    }
    else if(indexPath.row == 4)
    {
        _imageView.image = [ImageUtil imageWithImage:_Image withColorMatrix:colormatrix_gete];
    }
    else if(indexPath.row == 5)
    {
        _imageView.image = [ImageUtil imageWithImage:_Image withColorMatrix:colormatrix_ruise];
    }
    else if(indexPath.row == 6)
    {
        _imageView.image = [ImageUtil imageWithImage:_Image withColorMatrix:colormatrix_jiuhong];
    }
    else if(indexPath.row == 7)
    {
        _imageView.image = [ImageUtil imageWithImage:_Image withColorMatrix:colormatrix_qingning];
    }
    else if(indexPath.row == 8)
    {
        _imageView.image = [ImageUtil imageWithImage:_Image withColorMatrix:colormatrix_langman];
    }
    else if(indexPath.row == 9)
    {
        _imageView.image = [ImageUtil imageWithImage:_Image withColorMatrix:colormatrix_menghuan];
    }
    else
        _imageView.image = [ImageUtil imageWithImage:_Image withColorMatrix:colormatrix_landiao];
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *filtersTableViewCellIdentifier = @"filtersTableViewCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: filtersTableViewCellIdentifier];
    UIImageView *filterImageView;
    UIView *filterImageViewContainerView;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:filtersTableViewCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        if(KDeviceHeight > 480)
        {
            
        filterImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 90)];
            
        filterImageView.transform = CGAffineTransformMakeRotation(M_PI/2);
        filterImageView.tag = kFilterImageViewTag;
         
        filterImageViewContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 90)];
            filterImageViewContainerView.backgroundColor = [UIColor clearColor];
        filterImageViewContainerView.tag = kFilterImageViewContainerViewTag;
        [filterImageViewContainerView addSubview:filterImageView];
        }
        else
        {
            filterImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 80, 85)];
            filterImageView.backgroundColor = [UIColor clearColor];
            filterImageView.transform = CGAffineTransformMakeRotation(M_PI/2);
            filterImageView.tag = kFilterImageViewTag;
            
            filterImageViewContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 85)];
            filterImageViewContainerView.backgroundColor = [UIColor clearColor];
            filterImageViewContainerView.tag = kFilterImageViewContainerViewTag;
            [filterImageViewContainerView addSubview:filterImageView];

        }
        [cell.contentView addSubview:filterImageViewContainerView];
    } else {
        filterImageView = (UIImageView *)[cell.contentView viewWithTag:kFilterImageViewTag];
    }
    if(c == 1)
    {
        NSString *picname = [NSString stringWithFormat:@"f_%d.png",indexPath.row];
        
        filterImageView.image = [UIImage imageNamed:picname];
    }
    else if(c==2)
    {
        
        if(num == 1)
        {
            NSString *picname = [NSString stringWithFormat:@"%@",_sticker[indexPath.row]];
            
            filterImageView.image = [UIImage imageNamed:picname];
        }
        else
        {
            if(indexPath.row > 29)
            {
                NSString *picname = [NSString stringWithFormat:@"stamp_%d.png",indexPath.row];
        
                filterImageView.image = [UIImage imageNamed:picname];
            
            }
            else
            {
            NSString *picname = [NSString stringWithFormat:@"%d.png",indexPath.row];
            
            filterImageView.image = [UIImage imageNamed:picname];

            }
        }
    }
    
    else if(c == 4)
    {
        NSString *picname = [NSString stringWithFormat:@"c_%d.png",indexPath.row];
        
        filterImageView.image = [UIImage imageNamed:picname];
    }
    
    
    else
    {
        //NSString *picname = [NSString stringWithFormat:@"frame_%d.png",indexPath.row];
        NSString *picname = [NSString stringWithFormat:@"%@",_sticker[indexPath.row]];
        
        filterImageView.image = [UIImage imageNamed:picname];

    }
    
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(c == 1)
        return  11;
    else if(c == 2)
        if(num ==1 )
        {
            return _sticker.count;
        }
        else
         return 27;
    else if(c == 4)
        return 6;
    else
        return _sticker.count;
}
#pragma mark -


#pragma mark - Load
-(void) viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    float hframe;
    float hbutton;
    float hv;
    
    if(KDeviceHeight > 480)
    {
        hframe = 460-82;
        hbutton = -30;
        hv  = 0;//KDeviceHeight-78;
        _topv.frame = CGRectMake(0, KDeviceHeight-80, 320, 80);
        hi = 40;
        hitable = 90;
    }
    else
    {
        hframe = 379-39;
        hbutton = -50;
        hv  = 0;//KDeviceHeight-70;
        _topv.frame = CGRectMake(0, KDeviceHeight+4, 320, 67);

        if([[[UIDevice currentDevice] systemVersion] floatValue]>6.0f) hi = 10;
        else  hi = 0;
        hitable = 85;
    }

    
    tagview = 1;
    tag = 1000;
    _topv.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"menu1@2x.png"]];
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, hi, 320, 320)];
    _imageView.image = _Image;
    _imageView.userInteractionEnabled = YES;
    _imageView.clipsToBounds = YES;
    //_imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:_imageView];
    
    
    
    
    _imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer * singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    [_imageView addGestureRecognizer:singleTap];
    
    
    
    
    _localpinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch2:)];
    
    _localrotation = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotation2:)];
    
    
    
    _localrotation.delegate = self;
    
    [_imageView addGestureRecognizer:_localrotation];
    
    
    
    
    [_imageView addGestureRecognizer:_localpinch];
    
    
    
    
    
    self.filterTableViewContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, hframe, 320, hitable)];
    self.filterTableViewContainerView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"menu2@2x.png"]];
    
    
   // _topv.backgroundColor =
    
    
    c = 1;
    
    //_filterTable.frame = CGRectMake(124, -124, 72, 320) ;
//    self.filtersTableView = [[UITableView alloc] initWithFrame:CGRectMake(124, -124, 72, 320) style:UITableViewStylePlain];
//    self.filtersTableView.backgroundColor = [UIColor clearColor];
//    self.filtersTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.filtersTableView.showsVerticalScrollIndicator = NO;
//    self.filtersTableView.delegate = self;
//    self.filtersTableView.dataSource = self;
//    self.filtersTableView.transform	= CGAffineTransformMakeRotation(-M_PI/2);
    
    self.filtersTableView = [[UITableView alloc] initWithFrame:CGRectMake(124, -124, hitable, 320) style:UITableViewStylePlain];
    self.filtersTableView.backgroundColor = [UIColor clearColor];
    self.filtersTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.filtersTableView.showsVerticalScrollIndicator = NO;
    self.filtersTableView.delegate = self;
    self.filtersTableView.dataSource = self;
    self.filtersTableView.transform	= CGAffineTransformMakeRotation(-M_PI*0.5);
    [self.filtersTableView setFrame:CGRectMake(0 * 0.5, 0 * 0.5, 320, hitable)];

    //_im.image = _Image;
    [self.filterTableViewContainerView addSubview:self.filtersTableView];
    [self.view addSubview:self.filterTableViewContainerView];
    
    _frameImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 320, 320)];
    _frameImageView.backgroundColor = [UIColor clearColor];
    _frameImageView.userInteractionEnabled = NO;
    [self.imageView addSubview:_frameImageView];
    _stampbtn = [[UIButton alloc] init];
    _stampbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_stampbtn setFrame:CGRectMake(0,hv,wobj,wobj)];
    [_stampbtn setImageEdgeInsets:UIEdgeInsetsMake(0,2,0,0)];
    [_stampbtn setTitleEdgeInsets:UIEdgeInsetsMake(2,-2,0,0)];
    [_stampbtn setImage:[UIImage imageNamed:@"btn-2.png"] forState:UIControlStateNormal];
    [_stampbtn setTitle:@"" forState:UIControlStateNormal];
    [_stampbtn setTag:5];
    [_stampbtn addTarget:self action:@selector(BtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.topv addSubview:_stampbtn];

    
    int x = 53;
    int b = x;
    
        NSLog(@"%d",b);
    
    
    
    

     _fillter = [[UIButton alloc] init];
    _fillter = [UIButton buttonWithType:UIButtonTypeCustom];
    [_fillter setFrame:CGRectMake(b,hv,wobj,wobj)];
    [_fillter setImageEdgeInsets:UIEdgeInsetsMake(0,2,0,0)];
    [_fillter setTitleEdgeInsets:UIEdgeInsetsMake(2,-2,0,0)];
    [_fillter setImage:[UIImage imageNamed:@"btn-5.png"] forState:UIControlStateNormal];
    [_fillter setTitle:@"" forState:UIControlStateNormal];
    [_fillter setTag:6];
    [_fillter addTarget:self action:@selector(BtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    //_fillter.selected=YES;
    [self.topv addSubview:_fillter];
    b = b+x;
  
    
   
    _txtbtn = [[UIButton alloc] init];
    _txtbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_txtbtn setFrame:CGRectMake(b,hv,wobj,wobj)];
    [_txtbtn setImageEdgeInsets:UIEdgeInsetsMake(0,2,0,0)];
    [_txtbtn setTitleEdgeInsets:UIEdgeInsetsMake(2,-2,0,0)];
    [_txtbtn setImage:[UIImage imageNamed:@"btn-4.png"] forState:UIControlStateNormal];
    [_txtbtn setTitle:@"" forState:UIControlStateNormal];
    [_txtbtn setTag:8];
    [_txtbtn addTarget:self action:@selector(BtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.topv addSubview:_txtbtn];
    
    b=b+x;
    _framebtn = [[UIButton alloc] init];
    _framebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_framebtn setFrame:CGRectMake(b,hv,wobj,wobj)];
    [_framebtn setImageEdgeInsets:UIEdgeInsetsMake(0,2,0,0)];
    [_framebtn setTitleEdgeInsets:UIEdgeInsetsMake(2,-2,0,0)];
    [_framebtn setImage:[UIImage imageNamed:@"btn-3.png"] forState:UIControlStateNormal];
    [_framebtn setTitle:@"" forState:UIControlStateNormal];
    [_framebtn setTag:10];
    [_framebtn addTarget:self action:@selector(BtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.topv addSubview:_framebtn];

    b=b+x;
    
    _savebtn= [[UIButton alloc] init];
    _savebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_savebtn setFrame:CGRectMake(b,hv,wobj,wobj)];
    [_savebtn setImageEdgeInsets:UIEdgeInsetsMake(0,2,0,0)];
    [_savebtn setTitleEdgeInsets:UIEdgeInsetsMake(2,-2,0,0)];
    [_savebtn setImage:[UIImage imageNamed:@"btn-6.png"] forState:UIControlStateNormal];
    [_savebtn setTitle:@"" forState:UIControlStateNormal];
    [_savebtn setTag:7];
    [_savebtn addTarget:self action:@selector(BtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.topv addSubview:_savebtn];

        b=b+x;
    _backbtn = [[UIButton alloc] init];
    _backbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backbtn setFrame:CGRectMake(b,hv,wobj,wobj)];
    [_backbtn setImageEdgeInsets:UIEdgeInsetsMake(0,2,0,0)];
    [_backbtn setTitleEdgeInsets:UIEdgeInsetsMake(2,-2,0,0)];
    [_backbtn setImage:[UIImage imageNamed:@"btn-7.png"] forState:UIControlStateNormal];
    [_backbtn setTitle:@"" forState:UIControlStateNormal];
    [_backbtn setTag:1];
    [_backbtn addTarget:self action:@selector(BtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.topv addSubview:_backbtn];
   
    
    /*
    UIButton *rotateRightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rotateRightButton setImage:[UIImage imageNamed:@"imageviewer_toolbar_background.png"] forState:UIControlStateNormal];
    rotateRightButton.frame = CGRectMake(250, 2, 32, 31);
    
    [rotateRightButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
     */
/*
    UIButton *close = [UIButton buttonWithType:UIButtonTypeCustom];
    close.frame = CGRectMake(285, 2, 32, 31);
    [close setImageEdgeInsets:UIEdgeInsetsMake(0,2,0,0)];
    [close setTitleEdgeInsets:UIEdgeInsetsMake(2,-2,0,0)];
    
    [close setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [close setTitle:@"close" forState:UIControlStateNormal];
    [close setBackgroundImage:[[UIImage imageNamed:@"imageviewer_toolbar_background.png"]stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateNormal];
    
    [close addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    

    
    [self.view addSubview:close];
    */
    NSLog(@"%f",KDeviceHeight);
    
    
    
    
    UIButton *rotateRightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rotateRightButton setImage:[UIImage imageNamed:@"leftButton"] forState:UIControlStateNormal];
    rotateRightButton.frame = CGRectMake(91, hbutton, 32, 31);
    [rotateRightButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
     //   [self.view addSubview:rotateRightButton];
    UIButton *rotateLeftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rotateLeftButton setImage:[UIImage imageNamed:@"rightButton"] forState:UIControlStateNormal];
    rotateLeftButton.frame = CGRectMake(133, hbutton, 32, 31);
    
    [rotateLeftButton addTarget:self action:@selector(rotate:) forControlEvents:UIControlEventTouchUpInside];

    /*
    
    [rotateLeftButton addEventHandler:^(id sender){
        [UIView animateWithDuration:0.1 animations:^{
            tempSelf.selectedView.transform = CGAffineTransformRotate(CGAffineTransformRotate(tempSelf.selectedView.transform, -degreesToRadians(-45 * (tempSelf.selectedView.tag))), degreesToRadians(-45 * (++tempSelf.selectedView.tag)));
        }];
    } forControlEvents:UIControlEventTouchUpInside];
     */
    //[self.view addSubview:rotateLeftButton];

    UIButton *zoomInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [zoomInButton setImage:[UIImage imageNamed:@"plusButton"] forState:UIControlStateNormal];
    zoomInButton.frame = CGRectMake(9, hbutton, 31, 31);
    
    [zoomInButton addTarget:self action:@selector(plus:) forControlEvents:UIControlEventTouchUpInside];
    
   //[self.view addSubview:zoomInButton];
    
    UIButton *zoomOutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [zoomOutButton setImage:[UIImage imageNamed:@"minusButton"] forState:UIControlStateNormal];
    zoomOutButton.frame = CGRectMake(51, hbutton, 31, 31);
    [zoomOutButton addTarget:self action:@selector(mini:) forControlEvents:UIControlEventTouchUpInside];
    
    //[self.view addSubview:zoomOutButton];
    
    
    _deletebutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_deletebutton setImage:[UIImage imageNamed:@"deleteButton"] forState:UIControlStateNormal];
    _deletebutton.frame = CGRectMake(0, hbutton, 31, 31);
    [_deletebutton addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_deletebutton];
   //[self.view insertSubview:_deletebutton aboveSubview:_frameImageView];
    _deletebutton.hidden = YES;
    
    tagbutton = 10001;
    
    
    _dotDelete=[UIButton buttonWithType:UIButtonTypeCustom];
    _dotDelete.frame = CGRectMake(0, 0, 25, 25);
    //[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [_dotDelete setImage:[UIImage imageNamed:@"close_gold.png"]forState:UIControlStateNormal];
    _dotDelete.center=CGPointMake(_selectedView.bounds.origin.x+18, _selectedView.bounds.origin.y+15
                                  );
    _dotDelete.tag = 989999;
    _dotDelete.userInteractionEnabled = YES;
    //[dotDelete addGestureRecognizer:singleTap];
    
    [_dotDelete addTarget:self action:@selector(deletestamp:) forControlEvents:UIControlEventTouchUpInside];
    
    //[_selectedView addSubview:_dotDelete];
    
    
    
    [self BtnClicked:_fillter];

    // Do any additional setup after loading the view from its nib.
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void) cview
{
    /*for (int i = 0; i < [[self.view subviews] count]; i++ ) {
        [[[self.view subviews] objectAtIndex:boxIndex] removeFromSuperview];
    }*/
    NSArray *subviews = [self.view subviews];
    for (UIView *subview in subviews) {
        
        NSLog(@"%@", subview);
        
        // List the subviews of subview
        //[self listSubviewsOfView:subview];
    }
    NSLog(@"%d",[[self.view subviews] count]);
    //[[self.view viewWithTag:1000] removeFromSuperview];
    _selectedView = nil;
    
    if(tag == 1003)
    {
        [[self.view viewWithTag:1000] removeFromSuperview];
                [[self.view viewWithTag:1001] removeFromSuperview];
                [[self.view viewWithTag:1002] removeFromSuperview];
        tag = 1000;
    }
    
}
#pragma mark -

#pragma mark -Action
-(void)singleTap:(UIPanGestureRecognizer *)recognizer
{
    NSLog(@"%d",recognizer.view.tag);
    [self.view viewWithTag:10000].hidden = YES;
    
    [_dotDelete removeFromSuperview];
    
    _selectedView.backgroundColor = [UIColor clearColor];
    _selectedView.layer.borderColor = [UIColor clearColor].CGColor;
    //_v.backgroundColor = [UIColor greenColor];
    
   // UIButton *button = nil;
    NSLog(@"%d - %d",[_selectedView.subviews count],tagview);
//    int i =  tagview;
//  for (UIView *view in [_selectedView subviews])
//    {
//        
//        NSLog(@"subview %d",i);
//        button= (UIButton *)view;
//        [button viewWithTag:i].hidden = YES;
//        if([view isKindOfClass:[UIButton class]]) {
//            //do whatever you want to do
//            
//        }
//        
//       // (view *)[_selectedView viewWithTag:i].hidden = YES;
//        
//        //subview.backgroundColor = [UIColor whiteColor];
//        
//        
//        
//        
//        
//        //subview = nil;
//        //NSLog(@"xxx");
//        
//        
//        [self.view viewWithTag:i].hidden = YES;
//        i = i-1;
//        
//    }
    
    for(int x=9999;x<tagbutton;x++)
    {
        //[self.view viewWithTag:x].hidden = YES;
          [self.view viewWithTag:x].hidden = YES;
        
        
        
        
    }
    _selectedView = nil;
}
-(void)mini:(id)sender
{
    const CGFloat kMinScale = 0.5;
    CGFloat currentScale = [[_selectedView.layer valueForKeyPath:@"transform.scale"] floatValue];
    
    CGFloat newScale = 0.909;
    newScale = MAX(newScale, kMinScale / currentScale);
    
    if (currentScale > kMinScale) {
        [UIView animateWithDuration:0.1 animations:^{
            _selectedView.transform = CGAffineTransformScale(_selectedView.transform, newScale, newScale);
        }];
    }
}


-(void)delete:(id)sender
{
    [_selectedView removeFromSuperview];
    _selectedView = nil;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}
- (void)handleRotation2:(UIRotationGestureRecognizer *)recognizer {
    NSLog(@"ro");
	if(recognizer.state == UIGestureRecognizerStateEnded) {
		_lastRotation = 0.0;
		return;
	}
    
	CGFloat rotation = 0.0 - (_lastRotation - [recognizer rotation]);
    

    
	_selectedView.transform = CGAffineTransformRotate(_selectedView.transform, rotation);
    
	_lastRotation = [recognizer rotation];
}

- (void)handlePinch2:(UIPinchGestureRecognizer *)recognizer {
    if(recognizer.state == UIGestureRecognizerStateBegan) {
        // Reset the last scale, necessary if there are multiple objects with different scales
        if(_lastScale)
            NSLog(@"%f",_lastScale);
        else
        _lastScale = recognizer.scale;
    } else {
        CGFloat currentScale = [[_selectedView.layer valueForKeyPath:@"transform.scale"] floatValue];
        
        // Constants to adjust the max/min values of zoom
        const CGFloat kMaxScale = 3;
        const CGFloat kMinScale = 0.5;
        
        CGFloat newScale = 1 -  (_lastScale - recognizer.scale); // new scale is in the range (0-1)
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale(_selectedView.transform, newScale, newScale);
        _selectedView.transform = transform;
        
        _lastScale = [[_selectedView.layer valueForKeyPath:@"transform.scale"] floatValue];  // Store the previous scale factor for the next pinch gesture call
    }
}


-(void)plus:(id)sender
{
    const CGFloat kMaxScale = 3;
    CGFloat currentScale = [[_selectedView.layer valueForKeyPath:@"transform.scale"] floatValue];
    
    CGFloat newScale = 1.1;
    newScale = MIN(newScale, kMaxScale / currentScale);
    
    [UIView animateWithDuration:0.1 animations:^{
        
        _selectedView.transform = CGAffineTransformScale(_selectedView.transform, newScale, newScale);
    }];
}


-(void)buttonClicked:(id)sender
{
    [UIView animateWithDuration:0.1 animations:^{
        _selectedView.transform = CGAffineTransformRotate(CGAffineTransformRotate(_selectedView.transform, -degreesToRadians(45 * (_selectedView.tag))), degreesToRadians(45 * (++_selectedView.tag)));
    }];

}

-(void)left:(id)sender
{
    [UIView animateWithDuration:0.1 animations:^{
        _selectedView.transform = CGAffineTransformRotate(CGAffineTransformRotate(_selectedView.transform, -degreesToRadians(-45 * (_selectedView.tag))), degreesToRadians(-45 * (++_selectedView.tag)));
    }];
    
}


-(void)rotate:(id)sender
{
    [UIView animateWithDuration:0.1 animations:^{
        _imageView.transform = CGAffineTransformRotate(CGAffineTransformRotate(_imageView.transform, -degreesToRadians(90 * (_imageView.tag))), degreesToRadians(90 * (++_imageView.tag)));
    }];
    
}



-(void)close:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}





-(void) BtnClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSLog(@"%d",btn.tag);
    switch (btn.tag) {
            
        case 1:
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        }
            
        case 5:  //向左
        {
            NSLog(@"Click");
            //c = 2;
            //num = 1;
            [_stampbtn setImage:[UIImage imageNamed:@"btn-2-hover.png"] forState:UIControlStateNormal];
            [_fillter setImage:[UIImage imageNamed:@"btn-5.png"] forState:UIControlStateNormal];
            [_savebtn setImage:[UIImage imageNamed:@"btn-6.png"] forState:UIControlStateNormal];
            [_backbtn setImage:[UIImage imageNamed:@"btn-7.png"] forState:UIControlStateNormal];
            [_framebtn setImage:[UIImage imageNamed:@"btn-3.png"] forState:UIControlStateNormal];
            [_txtbtn setImage:[UIImage imageNamed:@"btn-4.png"] forState:UIControlStateNormal];
            //_sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"stamp" ofType:@"plist"]];
           // [self.filtersTableView reloadData];
            
//        _sticker = [NSArray arrayWithContentsOfFile:NIPathForBundleResource([NSBundle mainBundle],@"stamp.plist")];
//                _sticker = [[NSMutableArray alloc] init];

             //   _sticker = [[NSArray alloc] init];
            MViewStamp2 *viewController = [[MViewStamp2 alloc] initWithNibName:@"MViewStamp2" bundle:nil];
            viewController.delegate = self;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
            [self presentViewController:navigationController animated:YES completion:nil];
            
            break;
        }
        case 6:  //向左
        {
            NSLog(@"Click");
            c = 1;
            [_stampbtn setImage:[UIImage imageNamed:@"btn-2.png"] forState:UIControlStateNormal];
            [_fillter setImage:[UIImage imageNamed:@"btn-5-hover.png"] forState:UIControlStateNormal];
            [_savebtn setImage:[UIImage imageNamed:@"btn-6.png"] forState:UIControlStateNormal];
            [_backbtn setImage:[UIImage imageNamed:@"btn-7.png"] forState:UIControlStateNormal];
            [_framebtn setImage:[UIImage imageNamed:@"btn-3.png"] forState:UIControlStateNormal];
            [_txtbtn setImage:[UIImage imageNamed:@"btn-4.png"] forState:UIControlStateNormal];

            [self.filtersTableView reloadData];
            break;
        }
        case 7:
        {
            _selectedView.backgroundColor = [UIColor clearColor];
            //_selectedView.backgroundColor = [UIColor clearColor];
            _selectedView.layer.borderColor = [UIColor clearColor].CGColor;
            [_dotDelete removeFromSuperview];
            
            _selectedView = nil;
            
            //CGSize imagesize = CGSizeMake(320, 400);
            [_stampbtn setImage:[UIImage imageNamed:@"btn-2.png"] forState:UIControlStateNormal];
            [_fillter setImage:[UIImage imageNamed:@"btn-5.png"] forState:UIControlStateNormal];
            [_savebtn setImage:[UIImage imageNamed:@"btn-6-hover.png"] forState:UIControlStateNormal];
            [_backbtn setImage:[UIImage imageNamed:@"btn-7.png"] forState:UIControlStateNormal];
            [_framebtn setImage:[UIImage imageNamed:@"btn-3.png"] forState:UIControlStateNormal];
            [_txtbtn setImage:[UIImage imageNamed:@"btn-4.png"] forState:UIControlStateNormal];
//            UIGraphicsBeginImageContextWithOptions(_imageView.bounds.size, _imageView.opaque, 1.5);
//            [self.imageView.layer renderInContext:UIGraphicsGetCurrentContext()];
//            
//            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//            
            
            
            
            
            //UIView* captureView = self.view;
            
            UIGraphicsBeginImageContextWithOptions(_imageView.bounds.size, _imageView.opaque, 0.0);
            [self.imageView.layer renderInContext:UIGraphicsGetCurrentContext()];
            
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            
            /* Render the screen shot at custom resolution */
//            CGRect cropRect = CGRectMake(0 ,0 ,640 ,640);
//            UIGraphicsBeginImageContextWithOptions(cropRect.size, captureView.opaque, 1.0f);
//            [newImage drawInRect:cropRect];
//            UIImage * customScreenShot = UIGraphicsGetImageFromCurrentImageContext();
//            UIGraphicsEndImageContext();
//            
            /* Save to the photo album */
            //UIImageWriteToSavedPhotosAlbum(customScreenShot , nil, nil, nil);
            
            
            
            
            MShare *share = [[MShare alloc] initWithNibName:@"MShare" bundle:nil];
            
            share.Image = newImage;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:share];
            //now present this navigation controller as modally
            
            
            
            
           // [self presentModalViewController:navigationController animated:YES];

            
            
            [self presentViewController:navigationController animated:YES completion:nil];
            //[self presentModalViewController:last animated:YES];
            
            
            
            
            
            /*UIImageWriteToSavedPhotosAlbum(newImage, self, @selector(imageSavedToPhotosAlbum: didFinishSavingWithError: contextInfo:), nil);
            */
            
            
            /*
            
            
            
            UIGraphicsEndImageContext();
            NSArray *activityItems;
            activityItems = @[newImage];
            
            UIActivityViewController *activityController =
            [[UIActivityViewController alloc]
             initWithActivityItems:activityItems
             applicationActivities:nil];
            
            activityController.excludedActivityTypes = @[UIActivityTypePrint,UIActivityTypeAssignToContact, UIActivityTypePostToWeibo];
            
            
            [self presentViewController:activityController
                               animated:YES
                             completion:nil];
            
            activityController.completionHandler = ^(NSString *acType,BOOL complete)
            {
                if(complete)
                {
                    if([acType isEqualToString:@"com.apple.UIKit.activity.Mail"])
                    {
                        NSLog(@"You Send Mail");
                    }
                    else if([acType isEqualToString:@"com.apple.UIKit.activity.PostToFacebook"])
                    {
                       
                    }
                    else if([acType isEqualToString:@"com.apple.UIKit.activity.SaveToCameraRoll"])
                    {
                                            }
                    else if([acType isEqualToString:@"com.apple.UIKit.activity.PostToTwitter"])
                    {
                       
                    }
                    
                    else
                    {
                        NSLog(@"You Not Select");
                    }
                }
            };
            
            
            
             
            
            */
            
            break;
            
            
            
        }
        case 8:
        {
            _selectedView.backgroundColor = [UIColor clearColor];
            _selectedView = nil;
            //MTextView *t = [[MTextView alloc] initWithNibName:@"MTextView" bundle:nil];
            //t.delegate = self;
            //[self presentViewController:t animated:YES completion:nil];
            c = 4;
            [_stampbtn setImage:[UIImage imageNamed:@"btn-2.png"] forState:UIControlStateNormal];
            [_fillter setImage:[UIImage imageNamed:@"btn-5.png"] forState:UIControlStateNormal];
            [_savebtn setImage:[UIImage imageNamed:@"btn-6.png"] forState:UIControlStateNormal];
            [_backbtn setImage:[UIImage imageNamed:@"btn-7.png"] forState:UIControlStateNormal];
            [_framebtn setImage:[UIImage imageNamed:@"btn-3.png"] forState:UIControlStateNormal];
            [_txtbtn setImage:[UIImage imageNamed:@"btn-4-hover.png"] forState:UIControlStateNormal];
            [self.filtersTableView reloadData];
            break;
        }
        case 10:  //向左
        {
            NSLog(@"Click");
            c = 3;
            [_stampbtn setImage:[UIImage imageNamed:@"btn-2.png"] forState:UIControlStateNormal];
            [_fillter setImage:[UIImage imageNamed:@"btn-5.png"] forState:UIControlStateNormal];
            [_savebtn setImage:[UIImage imageNamed:@"btn-6.png"] forState:UIControlStateNormal];
            [_backbtn setImage:[UIImage imageNamed:@"btn-7.png"] forState:UIControlStateNormal];
            [_framebtn setImage:[UIImage imageNamed:@"btn-3-hover.png"] forState:UIControlStateNormal];
            [_txtbtn setImage:[UIImage imageNamed:@"btn-4.png"] forState:UIControlStateNormal];
            _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"frame" ofType:@"plist"]];
            
            
            [self.filtersTableView reloadData];
            
            
            
            break;
        }
        default:
            break;
    }
    
}

-(void)ADDText
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"ใส่ Text ได้เลยจ้า"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Continue", nil];
    
    [message setAlertViewStyle:UIAlertViewStylePlainTextInput];
    
    [message show];
    

    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
   NSString *content =  [[alertView textFieldAtIndex:0] text];
    
    NSLog(@"%@",content);
   // NSString *content = obj;
    // 计算出长宽
    UIFont *font = [UIFont fontWithName:@"Arial" size:30];
    CGSize size = [content sizeWithFont:font constrainedToSize:CGSizeMake(280, 220) lineBreakMode:NSLineBreakByTruncatingTail];
    
    NSLog(@"%f",size.height);
    
    NSLog(@"%f",size.width);
    NSLog(@"%d",color);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.text = content;

    switch (color)
    {
        case 0:
            label.textColor= [UIColor redColor];
            break;
        case 1:
            label.textColor =[UIColor blackColor];
            break;
        case 2:
            label.textColor=[UIColor greenColor];
            
            break;
        case 3:
            label.textColor=[UIColor blueColor];
            break;
        case 4:
            label.textColor=[UIColor purpleColor];
            break;
        case 5:
            label.textColor=[UIColor orangeColor];
            break;
        case 6:
            label.textColor=[UIColor grayColor];
            break;
        case 7:
            label.textColor=[UIColor purpleColor];
            break;
        case 8:
            
            label.textColor=[UIColor brownColor];
            break;
        case 9:
            label.textColor=[UIColor magentaColor];
            break;
        case 10:
            label.textColor=[UIColor whiteColor];
            break;
            
        default:
            break;
    }
    
       //label.textColor =  [UIColor magentaColor];
    
    label.font = [UIFont boldSystemFontOfSize:40];
    label.backgroundColor = [UIColor clearColor];
    //label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [label sizeToFit];
    
    ANDraggable *draggable = [[ANDraggable alloc] initWithFrame:label.frame];
    draggable.delegate = self;
    [draggable addSubview:label];
    [_imageView insertSubview:draggable belowSubview:_frameImageView];
    
    _selectedView = draggable;
    //_selectedView.backgroundColor = [UIColor grayColor];
    
    _deletebutton.hidden = NO;
    

}

-(void ) DoneClick:(id)sender

{
    if([_text.text length]> 1)
    {
        self.view = _Pic;
        CGFloat contentWidth = 280;
        // 设置字体
        UIFont *font = [UIFont fontWithName:@"Arial" size:10];
        
        NSLog(@"%d",color);
        
        switch (color)
        {
            case 0:
                segmentColor=[[UIColor blackColor] CGColor];
                break;
            case 1:
                segmentColor=[[UIColor redColor] CGColor];
                break;
            case 2:
                segmentColor=[[UIColor blueColor] CGColor];
                
                break;
            case 3:
                segmentColor=[[UIColor greenColor] CGColor];
                break;
            case 4:
                segmentColor=[[UIColor yellowColor] CGColor];
                break;
            case 5:
                segmentColor=[[UIColor orangeColor] CGColor];
                break;
            case 6:
                segmentColor=[[UIColor grayColor] CGColor];
                break;
            case 7:
                segmentColor=[[UIColor purpleColor]CGColor];
                break;
            case 8:
                
                segmentColor=[[UIColor brownColor]CGColor];
                break;
            case 9:
                segmentColor=[[UIColor magentaColor]CGColor];
                break;
            case 10:
                segmentColor=[[UIColor whiteColor]CGColor];
                break;
                
            default:
                break;
        }
        
        
        //NSDictionary *obj = [_movies objectAtIndex:row];
        
        //cell.txtAnchor.text = [obj objectForKey:@"firstname"];
        //cell.txtContent.text = [obj objectForKey:@"text"];
        //QiuShi *qs =[self.list objectAtIndex:row];
        // 显示的内容
        NSString *content = _text.text;
        // 计算出长宽
        CGSize size = [content sizeWithFont:font constrainedToSize:CGSizeMake(contentWidth, 220) lineBreakMode:NSLineBreakByTruncatingTail];
        
        NSLog(@"%f",size.height);
        
        NSLog(@"%f",size.width);
        
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.text = _text.text;
//        label.textColor = [UIColor colorWithRed:0.800 green:0.400 blue:0.400 alpha:1.000];
        label.textColor = (__bridge UIColor *)(segmentColor);
        label.font = [UIFont boldSystemFontOfSize:30];
        label.backgroundColor = [UIColor clearColor];
        label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [label sizeToFit];
        
        ANDraggable *draggable = [[ANDraggable alloc] initWithFrame:label.frame];
        draggable.delegate = self;
        [_imageView addSubview:label];
        // [self.imageView insertSubview:draggable belowSubview:_frameImageView];
        
        _selectedView = draggable;

    }
    
}
#pragma mark -

-(void) gotoStamp2:(int)obj with:(int)tagstamp
{
    c = 2;
    num = 1;

    
    NSLog(@"%d",tagstamp);
    if(tagstamp == 1)
    {
    _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"rabbit" ofType:@"plist"]];
    }
    else if(tagstamp == 2)
    {
        _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"panda" ofType:@"plist"]];
    }
    else if(tagstamp==3)
    {
        _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"dog" ofType:@"plist"]];
        
    }
    else if(tagstamp==4)
    {
        _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"cat" ofType:@"plist"]];
        
    }
    else 
    {
        _sticker =   [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"monkey" ofType:@"plist"]];
        
    }
    
    [self.filtersTableView reloadData];

    
    
    [self AddStamp:obj];
    
   // [self.filtersTableView reloadData];
}

-(void) gotoText2:(NSString *)obj
{
    //NSLog(@"%@",_text.text);
    CGFloat contentWidth = 280;
    // 设置字体
    UIFont *font = [UIFont fontWithName:@"Arial" size:14];
    //NSDictionary *obj = [_movies objectAtIndex:row];
    
    //cell.txtAnchor.text = [obj objectForKey:@"firstname"];
    //cell.txtContent.text = [obj objectForKey:@"text"];
    //QiuShi *qs =[self.list objectAtIndex:row];
    // 显示的内容
    NSString *content = obj;
    // 计算出长宽
    CGSize size = [content sizeWithFont:font constrainedToSize:CGSizeMake(contentWidth, 220) lineBreakMode:NSLineBreakByTruncatingTail];
    
    NSLog(@"%f",size.height);
    
    NSLog(@"%f",size.width);
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.text = obj;
    label.textColor = [UIColor colorWithRed:0.800 green:0.400 blue:0.400 alpha:1.000];
    label.font = [UIFont boldSystemFontOfSize:30];
    label.backgroundColor = [UIColor clearColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [label sizeToFit];
    
    ANDraggable *draggable = [[ANDraggable alloc] initWithFrame:label.frame];
    draggable.delegate = self;
    [draggable addSubview:label];
     [_imageView insertSubview:draggable belowSubview:_frameImageView];
    
    _selectedView = draggable;
    

    
}

/*
-(void) gotoText:(NSString *)obj
{
    //NSLog(@"%@",_text.text);
    CGFloat contentWidth = 280;
    // 设置字体
    UIFont *font = [UIFont fontWithName:@"Arial" size:14];
    //NSDictionary *obj = [_movies objectAtIndex:row];
    
    //cell.txtAnchor.text = [obj objectForKey:@"firstname"];
    //cell.txtContent.text = [obj objectForKey:@"text"];
    //QiuShi *qs =[self.list objectAtIndex:row];
    // 显示的内容
    NSString *content = obj;
    // 计算出长宽
    CGSize size = [content sizeWithFont:font constrainedToSize:CGSizeMake(contentWidth, 220) lineBreakMode:NSLineBreakByTruncatingTail];
    
    NSLog(@"%f",size.height);
    
    NSLog(@"%f",size.width);
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, size.width, size.height)];
    label.text = obj;
    label.textColor = [UIColor colorWithRed:0.800 green:0.400 blue:0.400 alpha:1.000];
    label.font = [UIFont boldSystemFontOfSize:30];
    label.backgroundColor = [UIColor clearColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [label sizeToFit];
    
    ANDraggable *draggable = [[ANDraggable alloc] initWithFrame:label.frame];
    draggable.delegate = self;
    [_imageView addSubview:label];
   // [self.imageView insertSubview:draggable belowSubview:_frameImageView];
    
    _selectedView = draggable;

    
    
}
*/


- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *) contextInfo
{
    NSString *message;
    NSString *title;
    if (!error) {
        title = @"Save To Album";
        message = [NSString stringWithFormat:@"Save Complete"];
    } else {
        title = @"失败提示";
        message = [error description];
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"ตกลง"
                                          otherButtonTitles:nil];
    [alert show];
    //[alert release];
}
-(void)deletestamp:(id)sender
{
    [_selectedView removeFromSuperview];
    _selectedView = nil;
}


#pragma Drag

- (void)draggableObjectDidMove:(SEDraggable *)object {
    
    
    //    UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    deleteBtn.frame = CGRectMake(150, -5, 20, 20);
    //    deleteBtn.tag = tagbutton;
    //    [deleteBtn setImage:[UIImage imageNamed:@"bin.png"]forState:UIControlStateNormal];
    //
    //    deleteBtn.layer.shadowColor = [[UIColor blackColor] CGColor];
    //    deleteBtn.layer.shadowOffset = CGSizeMake(0,4);
    //    deleteBtn.layer.shadowOpacity = 0.3;
    //    deleteBtn.userInteractionEnabled = YES;
    //[deleteBtn addGestureRecognizer:tabshare];
    
    
    //[deleteBtn addTarget:self action:@selector(plus:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    //[_selectedView bringSubviewToFront:dotDelete];
    
    //[_selectedView insertSubview:<#(UIView *)#> atIndex:<#(NSInteger)#>:dotDelete belowSubview:_imageView];

    NSLog(@"tag:%d",object.tag);
    
    for(int x=9999;x<tagbutton;x++)
    {
        //[self.view viewWithTag:x].hidden = YES;
        [self.view viewWithTag:x].hidden = YES;
        
        
        
        
    }
    
    _selectedView.layer.borderColor = [UIColor clearColor].CGColor;
    _selectedView.backgroundColor = [UIColor clearColor];
    _selectedView = object;
    
    
    _selectedView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.4];
    _selectedView.layer.borderColor = [UIColor whiteColor].CGColor;
    _selectedView.layer.borderWidth = 3.0f;
    
    
    
    if(_dotDelete == nil)
    {
    _dotDelete=[UIButton buttonWithType:UIButtonTypeCustom];
    _dotDelete.frame = CGRectMake(0, 0, 25, 25);
    //[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [_dotDelete setImage:[UIImage imageNamed:@"close_gold.png"]forState:UIControlStateNormal];
    _dotDelete.center=CGPointMake(_selectedView.bounds.origin.x+18, _selectedView.bounds.origin.y+15
                                  );
    _dotDelete.tag = 989999;
    _dotDelete.userInteractionEnabled = YES;
    //[dotDelete addGestureRecognizer:singleTap];
    
    [_dotDelete addTarget:self action:@selector(deletestamp:) forControlEvents:UIControlEventTouchUpInside];
        
        NSLog(@"View");
        
    }
    else
    {
        [_selectedView addSubview:_dotDelete];
        [_selectedView setHidden:NO];
        
    }
    
    
    
    int s = object.tag;
    
    
    
    
    int show = 10000+s;
    
    NSLog(@"tagb : %d - show: %d ",tagbutton,show);
    
    [_selectedView viewWithTag:show].hidden=NO;
    

       
    NSLog(@"xxxx %d",_selectedView.tag);
}
- (void)setSelectedView:(UIView *)selectedView {
    NSLog(@"V");
    if (_selectedView != selectedView) {
        _selectedView.alpha = 1;
        _selectedView = selectedView;
        _selectedView.alpha = 0.8;
        
       [self.imageView bringSubviewToFront:_selectedView];
        
        for (UIView *view in self.imageView.subviews) {
            if ([view.subviews.lastObject isKindOfClass:[UILabel class]]) {
                [self.imageView bringSubviewToFront:view];
                _selectedView.backgroundColor = [UIColor clearColor];
                           }
        }
    }
}


#pragma mark -delegate return stamp
-(void)back_stamp:(int)obj withpic:(int)pic
{
    NSLog(@"%d,%d",obj,pic);
}


#pragma mark-

- (void)viewWillDisappear:(BOOL)animated {
    
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setViewText:nil];
    [self setText:nil];
    [self setPic:nil];
    [self setTopv:nil];
    _Image = nil;
    _imageView = nil;
    _txtbtn = nil;
    _savebtn = nil;
    _selectedView = nil;
    [super viewDidUnload];
}
@end
