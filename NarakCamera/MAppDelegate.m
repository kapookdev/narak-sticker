//
//  MAppDelegate.m
//  NarakCamera
//
//  Created by mookapook on 4/5/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import "MAppDelegate.h"

#import "MViewController.h"
#import "DLCImagePickerController.h"
@implementation MAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    //self.viewController = [[MViewController alloc] initWithNibName:@"MViewController" bundle:nil];
   
    self.DLCImagePickerController = [[DLCImagePickerController alloc] initWithNibName:@"DLCImagePicker" bundle:nil];
    
    
    
    UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:self.DLCImagePickerController];
    n.navigationBar.barStyle = UIBarStyleBlackOpaque;
    n.navigationBar.tintColor =  [[UIColor alloc] initWithRed:204.0 / 255 green:0.0 / 255 blue:102.0 / 255 alpha:1.0];
    self.window.rootViewController = n;
    //sleep(3);
    [self.window makeKeyAndVisible];
    return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog(@"begin");
    [FBAppCall handleDidBecomeActiveWithSession:self.session];
    
                           
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    //return [FBSession.activeSession handleOpenURL:url];
    
    
    
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
withSession:self.session];
    
    
    
}



- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [FBSession.activeSession close];
}

@end
