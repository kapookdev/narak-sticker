//
//  MLastView.m
//  NarakCamera
//
//  Created by mookapook on 4/10/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import "MLastView.h"

@interface MLastView ()

@end

@implementation MLastView


-(void)back_stamp:(int)obj withpic:(int)pic
{
    NSLog(@"%d xx,%d xx",obj,pic);
    [self.delegate back_stamp:obj withpic:pic];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(showAlertView:)];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
   
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    self.navigationController.navigationBar.tintColor =  [[UIColor alloc] initWithRed:204.0 / 255 green:0.0 / 255 blue:102.0 / 255 alpha:1.0];
    
    self.title = @"Share";

        
    
    
    if (!FBSession.activeSession.isOpen) {
        NSLog(@"%d - %d",FBSessionStateCreatedTokenLoaded,FBSession.activeSession.state);
        if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
            NSLog(@"xxx");
            // even though we had a cached token, we need to login to make the session usable
            [FBSession.activeSession openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
                // we recurse here, in order to update buttons and labels
                [self updateView];
            }];
        
    }
    }

}


-(void) updateView
{
    if (FBSession.activeSession.isOpen) {
        NSLog(@"Load");
        _sharebtn.titleLabel.text = @"Share  Facebook";
        
    }
    else
    {
        NSLog(@"Not Load");
    }
}




-(void)promptUserWithAccountNameForStatusUpdate {
    //[self controlStatusUsable:NO];
    [[FBRequest requestForMe] startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
         if (!error) {
             
             NSLog(@"%@",user.name);
             _sharebtn.titleLabel.text = @"Share  Facebook";
             //[self postImageToFB];
             
             //[tmp release];
             
         }
         
         //[self controlStatusUsable:YES];
     }];
}


-(void)showAlertView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)PostPhoto:(UIButton *)sender
{

    if (FBSession.activeSession.isOpen) {
        
        // Yes, we are open, so lets make a request for user details so we can get the user name.
        
        [self promptUserWithAccountNameForStatusUpdate];
        
        
        
    } else {
        
        // We don't have an active session in this app, so lets open a new
        // facebook session with the appropriate permissions!
        
        // Firstly, construct a permission array.
        // you can find more "permissions strings" at http://developers.facebook.com/docs/authentication/permissions/
        // In this example, we will just request a publish_stream which is required to publish status or photos.
        
//        NSArray *permissions = [[NSArray alloc] initWithObjects:
//                                @"publish_stream",
//                                nil];
        //[self controlStatusUsable:NO];
        // OPEN Session!
        /*
        [FBSession openActiveSessionWithPermissions:permissions
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState status,
                                                      NSError *error) {
                                      // if login fails for any reason, we alert
                                      if (error) {
                                          
                                          // show error to user.
                                          
                                      } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                                          
                                          // no error, so we proceed with requesting user details of current facebook session.
                                          
                                          [self promptUserWithAccountNameForStatusUpdate];
                                      }
                                      //[self controlStatusUsable:YES];
                                  }];
    }
*/
    }
}

-(void)postImageToFB
{

//    [FBRequestConnection startForUploadPhoto:_Image
//                           completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                               if (!error) {
//                                   UIAlertView *tmp = [[UIAlertView alloc]
//                                                       initWithTitle:@"Success"
//                                                       message:@"Photo Uploaded"
//                                                       delegate:self
//                                                       cancelButtonTitle:nil
//                                                       otherButtonTitles:@"Ok", nil];
//                                   
//                                   [tmp show];
//                                   //[tmp release];
//                               } else {
//                                   UIAlertView *tmp = [[UIAlertView alloc]
//                                                       initWithTitle:@"Error"
//                                                       message:@"Some error happened"
//                                                       delegate:self
//                                                       cancelButtonTitle:nil
//                                                       otherButtonTitles:@"Ok", nil];
//                                   
//                                   [tmp show];
//                                   //[tmp release];
//                               }
//                               
//                               //[self controlStatusUsable:YES];
//                           }];
    
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:@"Test Post My App" forKey:@"message"];
    [params setObject:UIImagePNGRepresentation(_Image) forKey:@"picture"];
    //_shareToFbBtn.enabled = NO; //for not allowing multiple hits
    
    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
     {
         if (error)
         {
             //showing an alert for failure
             //[self alertWithTitle:@"Facebook" message:@"Unable to share the photo please try later."];
             NSLog(@"%@",error);
             
         }
         else
         {
             //showing an alert for success
             //[UIUtils alertWithTitle:@"Facebook" message:@"Shared the photo successfully"];
             
             UIAlertView *tmp = [[UIAlertView alloc]
                                 initWithTitle:@"Success"
                                 message:@"Photo Uploaded"
                                 delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:@"Ok", nil];
             
             [tmp show];
             
         }
         //_shareToFbBtn.enabled = YES;
     }];
}



- (IBAction)SaveCamera:(id)sender {
    MListStamp *l = [[MListStamp alloc] initWithNibName:@"MListStamp" bundle:nil];
    l.delegate = self;
    [self.navigationController pushViewController:l animated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setShareFaceBook:nil];
    [self setLoginFaceBook:nil];
    [self setSharebtn:nil];
    [super viewDidUnload];
}
@end
