//
//  MShare.h
//  NarakCamera
//
//  Created by mookapook on 4/19/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

#import "SSZipArchive.h"
//@class MAppDelegate;
@interface MShare : UIViewController<UITableViewDataSource,UITableViewDelegate,UIDocumentInteractionControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MBProgressHUDDelegate>
{
    UIDocumentInteractionController *dicont;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic)  UIImage *Image;
@property (strong, nonatomic)  NSArray *shareGO;
@property (strong, nonatomic) MBProgressHUD *HUD;
//@property (strong, nonatomic) MAppDelegate *appDelegate;
@end
